#![deny(unsafe_code)]
// warnings are ok during development, but have to be explicitly allowed at usage site if required during release
// #![cfg_attr(not(debug_assertions), deny(warnings))]
#![warn(clippy::pedantic)]
#![warn(clippy::nursery)]
#![warn(clippy::cargo)]
#![allow(clippy::cargo_common_metadata)]
// this is triggered by external dependencies and can't be fixed by us
#![allow(clippy::multiple_crate_versions)]
// sorry, good documentation is yet-to-come
#![allow(clippy::missing_panics_doc)]

use clap::Clap;
use std::env;

const RUST_LOG: &str = "RUST_LOG";

#[derive(Clap)]
#[clap(
	version = "0.0.0",
	author = "František Hanzlík <frantisekhanzlikbl@gmail.com>"
)]
pub struct GlobalOpts {
	/// the network interface to use for communication
	#[clap(short, long, default_value = "slcan0")]
	pub interface: String,
	#[clap(subcommand)]
	pub subcommand: Subcommand,
}

#[derive(Clap)]
pub enum Subcommand {
	Stop,
	Start(StartOpts),
}

#[derive(Clap)]
pub struct StartOpts {
	/// the serial device to communicate with
	#[clap(short, long, default_value = "/dev/ttyUSB0")]
	pub device: String,
}

pub fn init() -> GlobalOpts {
	if env::var_os(RUST_LOG).is_none() {
		env::set_var(RUST_LOG, "xtask=info");
	}
	pretty_env_logger::init_custom_env(RUST_LOG);

	GlobalOpts::parse()
}
